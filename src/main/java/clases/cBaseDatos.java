/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
        
package clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author RONALD
 */
public class cBaseDatos {

    String driver = "com.mysql.jdbc.Driver";
    String url = "jdbc:mysql://localhost:3306/nuevadb100";
    String usuario = "root";
    String clave = "";
    
    private Connection Conectar(){
        try{
            Class.forName(driver);
            Connection xcon = DriverManager.getConnection(url, usuario, clave);
            return xcon;
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
        
        return null;
    }
    
    public void listarPrueba() throws SQLException{
        try{
            Connection con = DriverManager.getConnection(url, usuario, clave);
            Statement stm = con.createStatement();
            ResultSet res = stm.executeQuery("SELECT * from tabla100");
            
            while(res.next()){
                String xcod = res.getString(1);
                String xnom = res.getString(2);
                System.out.println(xcod + " - " + xnom);  
            }
            
            System.out.print("OK");
        }catch(SQLException e){
            System.out.println(e.getCause() + "\n" + e.getMessage());
        }
        
    }
}
